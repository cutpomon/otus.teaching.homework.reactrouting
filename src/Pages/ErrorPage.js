import { Typography } from "@mui/material";

export default function ErrorPage() {
	return (
		<Typography
			variant="h1"
			sx={{
				justifyContent: "center",
				alignContent: "center",
				display: "flex",
			}}
		>
			Page not Found 404
		</Typography>
	);
}
