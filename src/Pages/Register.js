import { Container, Box, Typography, Button, TextField } from "@mui/material";

export default function Register() {
	const handleSubmit = (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		console.log({
			name: data.get("name"),
			surname: data.get("surname"),
			lastname: data.get("lastName"),
			email: data.get("email"),
			password: data.get("password"),
		});
	};

	return (
		<Container component="main" maxWidth="xs">
			<Box
				sx={{
					marginTop: 8,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}
			>
				<Typography component="h1" variant="h5">
					Регистрация
				</Typography>
				<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
					<TextField
						margin="normal"
						required
						fullWidth
						label="Имя"
						name="name"
						id="name"
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						label="Фамилия"
						name="surname"
						id="surname"
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						label="Отчество"
						name="lastName"
						id="lastName"
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						id="email"
						label="E-mail"
						name="email"
						autoComplete="email"
						autoFocus
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						name="password"
						label="Пароль"
						type="password"
						id="password"
						autoComplete="current-password"
					/>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						sx={{ mt: 3, mb: 2 }}
					>
						Зарегистрироваться
					</Button>
				</Box>
			</Box>
		</Container>
	);
}
