import { Container, Box, Typography, Button, ButtonGroup } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { Actions } from "../Store/Reducers/countReducer";

export default function Home() {
	const counter = useSelector((store) => store.count.homeCounter);

	const dispatch = useDispatch();

	const onIncreaseClick = (increaseBy) => {
		dispatch(Actions.increaseCount(increaseBy));
	};
	const onDecreaseClick = (decreaseBy) => {
		dispatch(Actions.decreaseCount(decreaseBy));
	};

	const onResetClick = () => {
		dispatch(Actions.resetCount());
	};

	return (
		<Container component="main" maxWidth="lg">
			<Box
				sx={{
					marginTop: 8,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}
			>
				<Typography component="h1" variant="h4" mb={8}>
					Добро пожаловать на главную страницу
				</Typography>
				<Typography variant="h3" mt={20} mb={5}>
					Счетчик: {counter}
				</Typography>
				<ButtonGroup
					size="large"
					sx={{
						display: "flex",
						flexDirection: "row",
						alignItems: "center",
					}}
				>
					<Button onClick={() => onIncreaseClick(1)}> Прибавить 1 </Button>
					<Button onClick={() => onIncreaseClick(5)}> Прибавить 5 </Button>
					<Button onClick={() => onDecreaseClick(1)}> Вычесть 1 </Button>
					<Button onClick={() => onDecreaseClick(5)}> Вычесть 5 </Button>
					<Button onClick={onResetClick}> Обнулить </Button>
				</ButtonGroup>
			</Box>
		</Container>
	);
}
