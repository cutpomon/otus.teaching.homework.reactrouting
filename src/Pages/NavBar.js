import {
	AppBar,
	Toolbar,
	IconButton,
	Typography,
	Stack,
	Button,
} from "@mui/material";
import { Link } from "react-router-dom";
import BuildIcon from "@mui/icons-material/Build";

export default function NavBar() {
	return (
		<div className="NavBar">
			<AppBar position="static">
				<Toolbar>
					<Link to="/" style={{ color: "inherit" }}>
						<IconButton
							size="large"
							edge="start"
							color="inherit"
							aria-label="logo"
						>
							<BuildIcon />
						</IconButton>
					</Link>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						Fancy React App
					</Typography>
					<Stack direction="row" spacing={2}>
						<Link to="/" style={{ color: "inherit" }}>
							<Button color="inherit">Главная</Button>
						</Link>
						<Link to="/login" style={{ color: "inherit" }}>
							<Button color="inherit">Войти</Button>
						</Link>
						<Link to="/register" style={{ color: "inherit" }}>
							<Button color="inherit">Зарегистрироваться</Button>
						</Link>
					</Stack>
				</Toolbar>
			</AppBar>
		</div>
	);
}
