const initialState = {
	homeCounter: 0,
};

const CountState = Object.freeze({
	INCR: "[COUNT_STATE] INCREASE_COUNT",
	DECR: "[COUNT_STATE] DECREASE_COUNT",
	RESET: "[COUNT_STATE] RESET_COUNT",
});

export const Actions = Object.freeze({
	increaseCount: (increaseBy) => ({
		type: CountState.INCR,
		payload: increaseBy,
	}),
	decreaseCount: (decreaseBy) => ({
		type: CountState.DECR,
		payload: decreaseBy,
	}),
	resetCount: () => ({
		type: CountState.RESET,
	}),
});

const countReducer = (state = initialState, action) => {
	switch (action.type) {
		case CountState.INCR:
			const newIncreasedState = {
				...state,
				homeCounter: state.homeCounter + action.payload,
			};
			return newIncreasedState;
		case CountState.DECR:
			const newDecreasedState = {
				...state,
				homeCounter: state.homeCounter - action.payload,
			};
			return newDecreasedState;
		case CountState.RESET:
			const newResetedState = {
				...state,
				homeCounter: 0,
			};
			return newResetedState;
		default:
			return state;
	}
};

export default countReducer;
